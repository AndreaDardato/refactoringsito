let pressure = document.getElementsByName("");
let ora = document.getElementsByName("ora");
let minPressure = document.getElementsByName("min");
let maxPressure = document.getElementsByName("max");
let yData = [];
let xData = [];

let minVal = minPressure[0].innerText;
let maxVal = maxPressure[0].innerText;

//console.log("pressure: ", pressure)

let lengthOfIDs = document.getElementById("numberOfIteration");

//get all pressure values
for(var i = 0; i < lengthOfIDs.innerText; i++) {
    yData.push(pressure[i].innerText);
}

//get all hours values
for(var j = 0; j < lengthOfIDs.innerText; j++) {
    xData.push(ora[j].innerText);
}

//creating my chart
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: xData,
        datasets: [{
            label: 'Pressure',
            data: yData,
            borderColor: ['red'],
            broderWidth: 1,
            fill: 0,
            responsive: true,
        }]
    },
    options: {
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                scaleOverride: true,
                min: parseInt(minVal-10),
                max: parseInt(maxVal+10),
                stepSize: 0.5,
            }]
        }
    }
});


