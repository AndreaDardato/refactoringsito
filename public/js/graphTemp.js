let temp = document.getElementsByName("");
let ora = document.getElementsByName("ora");
let minTemp = document.getElementsByName("min");
let maxTemp = document.getElementsByName("max");
var yData = [];
var xData = [];

let lengthOfIDs = document.getElementById("numberOfIteration")

console.log("temp: ", temp);
console.log("temp values: ", temp[0].innerText);
console.log("lengthOfIDs: ", lengthOfIDs.innerText);

let minVal = minTemp[0].innerText;
let maxVal = maxTemp[0].innerText;

for(var i = 0; i < lengthOfIDs.innerText; i++) {  //ciclo per recuperare i valori della temperatura
    console.log(temp[i].innerText);
    yData.push(temp[i].innerText);
}

for(var i = 0; i < lengthOfIDs.innerText; i++) {
    console.log(ora[i].innerText);
    xData.push(ora[i].innerText);
}

console.log("temp data: ", yData);
console.log("ora data: ", xData);

var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: xData,
        datasets: [{
            label: 'temperature',
            data: yData,
            borderColor: ['red'],
            broderWidth: 1,
            fill: 0,
            responsive: true,
        }]
    },
    options: {
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                scaleOverride: true,
                min: parseInt(minVal)-10,
                max: parseInt(maxVal)+10,
                stepSize: 0.5,
            }]
        }
    }
});
