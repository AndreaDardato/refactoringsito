let humid = document.getElementsByName("humid");
let ora = document.getElementsByName("ora");
let minHumid = document.getElementsByName("min");
let maxHumid = document.getElementsByName("max");
let yData = [];
let xData = [];

let minVal = minHumid[0].innerText;
let maxVal = maxHumid[0].innerText;

console.log("humid: ", humid)

let lengthOfIDs = document.getElementById("numberOfIteration");

//get all humid values
for(var i = 0; i < lengthOfIDs.innerText; i++) {
    yData.push(humid[i].innerText);
}

//get all hours values
for(var j = 0; j < lengthOfIDs.innerText; j++) {
    xData.push(ora[j].innerText);
}

//creating my chart
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: xData,
        datasets: [{
            label: 'humidity',
            data: yData,
            borderColor: ['green'],
            broderWidth: 1,
            fill: 0,
            responsive: true,
        }]
    },
    options: {
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                scaleOverride: true,
                min: parseInt(minVal-10),
                max: parseInt(maxVal+10),
                stepSize: 0.5,
            }]
        }
    }
});


