const mysql = require('mysql');

const DB = mysql.createConnection({
    host: '127.0.0.1',
    user: 'andrea',
    password: '',
    database: 'METEO',
    insecureAuth : true
})

exports.renderHomePage = (req, res) => {

    //create query for take the last misuration
    let query = "SELECT * FROM Station WHERE dateAndTime = (SELECT max(dateAndTime) FROM Station);"

    DB.query(query, (err, result) => {

        if(err) {
            throw err;
        }

        let dateAndTime = result[0].dateAndTime;
        let dataOdierna = result[0].DataOdierna;
        let ora = result[0].Ora;
        let temperatura = result[0].Temperatura;
        let umidita = result[0].Umidita;
        let pressione = result[0].Pressione;

        //console.log("result: ", result);

        console.log("Temperatura: ", temperatura);

        res.render('homePage', {
            "dateAndTime": dateAndTime,
            "dataOdierna": dataOdierna,
            "ora": ora,
            "temperatura": temperatura,
            "umidita": umidita,
            "pressione": pressione,
        })

    })
}


exports.renderGraphTempPage = (req, res) => {

    //take data from that day
    let query = "SELECT Temperatura, Ora FROM Station WHERE DataOdierna = (SELECT max(DataOdierna) FROM Station);";

    DB.query(query, (err, result) => {

        if(err) {
            throw err;
        }

        //console.log("Result = ", result);

        let listOfItems = [];

        for(var i = 0; i < result.length; i++) {
            console.log("dato numero ", i + 1, ": ", result[i]);
            listOfItems.push(i);
        }

        let secondaQuery = "SELECT min(Temperatura) as minTemp, max(Temperatura) as maxTemp FROM Station WHERE DataOdierna = (SELECT MAX(DataOdierna) FROM Station);"
        DB.query(secondaQuery, (err, secRes) => {

            if(err) {
                throw err;
            }

            //console.log("Second Result = ", secRes[0]);

            res.render('graphTemp', {
                "result": result,
                'secRes': secRes,
                'lengthOfIDs': listOfItems.length,
            })

        });

    });


}

exports.renderGraphHumidPage = (req, res) => {

    //take the last data of humidity
    let query = "SELECT Umidita, Ora FROM Station WHERE DataOdierna = (SELECT max(DataOdierna) FROM Station);";
    //now I'll execute the query
    DB.query(query, (err, result) => {

        if(err) {
            throw err;
        }

        //console.log("result: ", result);

        let listOfItems = [];

        for(var i = 0; i < result.length; i++) {
            listOfItems.push(i);    //listOfItems.length = number of elements
        }

        //select max and min values of humidity
        let secondaQuery = "SELECT max(Umidita) as maxHumid, min(Umidita) as minHumid FROM Station WHERE DataOdierna = (SELECT max(DataOdierna) FROM Station);";
        //call query function from mysql class
        DB.query(secondaQuery, (err, secRes) => {

            //controll if there are some errors
            if(err) {
                throw err;
            }

            //console.log("secRes: ", secRes);

            //now rendering the page
            res.render('graphHumid', {
                'result': result,
                'secRes': secRes,
                'lengthOfIDs': listOfItems.length,
                'isMonth': false,
                "title": "VALORI GIORNALIERI",
            });

        });

    });

}

exports.renderGraphPressurePage = (req, res) => {

    //take all data of the last day of misurations
    let query = "SELECT Pressione, Ora FROM Station WHERE DataOdierna = (SELECT max(DataOdierna) FROM Station);";
    
    //now Execute the query
    DB.query(query, (err, result) => {

        //check if there are some errors
        if(err) {
            throw err;
        }

        let listOfItems = [];

        for(var i = 0; i < result.length; i++) {
            listOfItems.push(i);    //listOfItems.length = number of elements
        }

        //select max and min values for pressure
        let secondQuery = "SELECT max(Pressione) as maxPress, min(Pressione) as minPress FROM Station WHERE DataOdierna = (SELECT max(DataOdierna) FROM Station);";
        //now Execute the query
        DB.query(secondQuery, (err, secRes) => {

            //check if there are some errors
            if(err) {
                throw err;
            }

            res.render('graphPressure', {
                'result': result,
                'secRes': secRes,
                'lengthOfIDs': listOfItems.length,
                'title': 'VALORI GIORNALIERI',
            });

        });

    });


}

exports.renderGraphTempMensilePage = (req, res) => {

    //voglio prendere i dati mensili
    let query = "SELECT Temperatura, Ora FROM Station WHERE DataOdierna BETWEEN (DataOdierna-30) AND DataOdierna;";
    DB.query(query, (err, result) => {
        
        if(err) {
            throw err;
        }

        //console.log("result: ", result);

        let listOfItems = [];

        for(let i = 0; i < result.length; i++) {
            listOfItems.push(i);
        }

        let secondaQuery = "SELECT max(Temperatura) as maxTemp, min(Temperatura) as minTemp FROM Station WHERE DataOdierna BETWEEN (DataOdierna-30) AND DataOdierna;";
        DB.query(secondaQuery, (err, secRes) => {

            if(err) {
                throw err;
            }

            //console.log("Second Result: ", secRes);

            res.render('graphTempMensile', {
                "result": result,
                "secondResult": secRes,
                "lengthOfIDs": listOfItems.length,
                'title': "DATI MENSILI"
            });

        });

    })

};

exports.renderGraphTempSettPage = (req, res) => {

    let query = `SELECT Temperatura, Ora FROM Station WHERE DataOdierna BETWEEN (DataOdierna-7) AND DataOdierna;`;
    DB.query(query, (err, result) => {

        if(err) {
            throw err;
        }

        //console.log("result: ", result);

        let listOfItems = [];

        for(let i = 0; i < result.length; i++) {
            listOfItems.push(i);
        }

        let secondaQuery = `SELECT max(Temperatura) as maxTemp, min(Temperatura) as minTemp FROM Station WHERE DataOdierna BETWEEN (DataOdierna-7) AND DataOdierna;`;
        DB.query(secondaQuery, (err, secRes) => {

            if(err) {
                throw err;
            }

            res.render('graphTempSettimanale', {
                "result": result,
                "secondResult": secRes,
                "lengthOfIDs": listOfItems.length,
                "title": "DATI SETTIMANALI"
            });

        });

    });

}

exports.renderGraphHumidMensilePage = (req, res) => {

    let query = `SELECT Umidita, Ora FROM Station WHERE DataOdierna BETWEEN (DataOdierna-30) AND DataOdierna; `;

    DB.query(query, (err, result) => {

        if(err) {
            throw err;
        }

        let listOfItems = [];

        for(let i = 0; i < result.length; i++) {
            listOfItems.push(i);
        }

        let secondaQuery = `SELECT max(Umidita) as maxHumid, min(Umidita) as minHumid FROM Station WHERE DataOdierna BETWEEN (DataOdierna-30) AND DataOdierna;`;

        DB.query(secondaQuery, (err, secRes) => {
            
            if(err) {
                throw err;
            }

            res.render('graphHumid', {
                "result": result,
                "secRes": secRes,
                "lengthOfIDs": listOfItems.length,
                "isMonth": true,
                "title": "VALORI MENSILI",
            })

        });

    })

}

exports.renderGraphHumidSettPage = (req, res) => {

    let query = `SELECT Umidita, Ora FROM Station WHERE DataOdierna BETWEEN (DataOdierna-7) AND DataOdierna;`;

    DB.query(query, (err, result) => {

        if(err) {
            throw err;
        }

        //console.log("result: ", result);

        let listOfItems = [];

        for(let i = 0; i < result.length; i++) {
            listOfItems.push(i);
        }
        
        let secondaQuery = `SELECT max(Temperatura) as maxHumid, min(Temperatura) as minHumid FROM Station WHERE DataOdierna BETWEEN (DataOdierna-7) AND DataOdierna;`;

        DB.query(secondaQuery, (err, secRes) => {

            if(err) {
                throw err;
            }

            res.render('graphHumidSett', {
                "result": result,
                "secondResult": secRes,
                "lengthOfIDs": listOfItems.length,
                "isMonth": false,
                "title": "VALORI SETTIMANALI",
            })

        })

    });

}

exports.renderGraphPressureMensilePage = (req, res) => {

    let query = `SELECT Pressione, Ora FROM Station WHERE DataOdierna BETWEEN (DataOdierna-30) AND DataOdierna;`;

    DB.query(query, (err, result) => {

        if(err) {
            throw err;
        }

        let listOfItems = [];

        for(let i = 0; i < result.length; i++) {
            listOfItems.push(i);
        }

        let secondaQuery = `SELECT max(Pressione) as maxPressure, min(Pressione) as minPressure FROM Station WHERE DataOdierna BETWEEN (DataOdierna-30) AND DataOdierna;`;

        DB.query(secondaQuery, (err, secRes) => {

            if(err) {
                throw err;
            }

            res.render('graphPressureMensile', {
                'result': result,
                'secRes': secRes,
                'lengthOfIDs': listOfItems.length,
                'title': 'VALORI MENSILI'
            })

        });

    });

}

exports.renderGraphPressureSettPage = (req, res) => {

    let query = `SELECT Pressione, Ora FROM Station WHERE DataOdierna BETWEEN (DataOdierna-7) AND DataOdierna;`;

    DB.query(query, (err, result) => {

        if(err) {
            throw err;
        }

        let listOfItems = [];

        for(let i = 0; i < result.length; i++) {
            listOfItems.push(i);
        }

        let secondaQuery = `SELECT max(Pressione) as maxPressure, min(Pressione) as minPressure FROM Station WHERE DataOdierna BETWEEN (DataOdierna-7) AND DataOdierna;`;

        DB.query(secondaQuery, (err, secRes) => {

            if(err) {
                throw err;
            }

            res.render('graphPressureSett', {
                'result': result,
                'secRes': secRes,
                'lengthOfIDs': listOfItems.length,
                'title': 'VALORI SETTIMANALI'
            });

        })

    });

}

