const express = require('express');

const app = express();
const PORT = 3000;
const path = require('path');
const router = require('./router');

app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.use(express.static("public"))

app.use(express.static(__dirname + '/public/'));


app.set('views', 'views');
app.set('view engine', 'hbs');

app.use('/', router);

app.listen(PORT, () => {
    console.log("Listening on port ", PORT);
})