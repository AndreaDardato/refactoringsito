const express = require('express');
const router = express.Router();
const controller = require('./controllers/controller');

router.get('/', controller.renderHomePage);

router.get('/graph/temp', controller.renderGraphTempPage);

router.get('/graph/humid', controller.renderGraphHumidPage);

router.get('/graph/pressure', controller.renderGraphPressurePage);

router.get('/graph/temp/mensile', controller.renderGraphTempMensilePage);

router.get('/graph/temp/sett', controller.renderGraphTempSettPage);

router.post('/graph/humid', controller.renderGraphHumidMensilePage);

router.get('/graph/humid/sett', controller.renderGraphHumidSettPage);

router.get('/graph/pressure/mensile', controller.renderGraphPressureMensilePage);

router.get('/graph/pressure/sett', controller.renderGraphPressureSettPage);

module.exports = router;